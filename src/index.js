import { fetchPosts, selectPosts, renderPosts } from './js/posts';

// Styles
import './styles/css/style.css';
import './styles/scss/main.scss';

// Js files
import './js/jquery';
import './js/highlight.min';
import './js/script';

// DOM Elements
const btnNextPosts = document.getElementById('btn-next-posts');
const postsContainer = document.getElementById('posts-container');

// Posts
fetchPosts().then(data => {
    const posts = data.reverse();
    const postsToRender = selectPosts(posts, 1);
    renderPosts(postsToRender.data);

    btnNextPosts.style.display = 'inline-block';

    btnNextPosts.addEventListener('click', (e) => {
        const nextPage = parseInt(e.target.dataset.next, 10);
        const nextPosts = selectPosts(posts, nextPage);
        renderPosts(nextPosts.data);

        if (!nextPosts.hasNext) {
            e.target.style.display = 'none';
        } else {
            e.target.setAttribute('data-next', nextPage + 1);
        }

    });

}).catch(err => {
    console.log(err);
    postsContainer.innerHTML = '<p class="blog__error-message">Something went wrong... Please try to refresh page or check your Internet connection...</p>';
});



