const fetchPosts = async () => {
    const response = await fetch('https://jsonplaceholder.typicode.com/posts/ ');

    if (response.status === 200) {
        return response.json();
    }

    throw new Error('Can\'t fetch data!');

};


const selectPosts = (posts, pageNumber, pageSize = 10) => {
    const startIndex = (pageNumber - 1) * pageSize;
    const endIndex = startIndex + pageSize;

    return {
        data: posts.slice(startIndex, endIndex),
        hasNext: endIndex < posts.length
    }
};

const generatePostTemplate = ({ title, body }) => (` <li
                                                        class="blog__item blog-item column--xs-12 column--md-6"
                                                    >
                                                        <a href="javascript:void(0);" class="blog-item__title">${title}</a>
                                                        <p class="blog-item__body">${body}</p>
                                                    </li>`);


const renderPosts = (posts) => {
    if (!Array.isArray(posts)) return;

    const postsContainer = document.getElementById('posts-container');

    if (!postsContainer) return;

    const postsToRender = posts.map(post => generatePostTemplate(post)).join('');

    postsContainer.insertAdjacentHTML('beforeend', postsToRender);
};

export { fetchPosts, selectPosts, renderPosts };